import os
from PIL import Image, ImageDraw, ImageFont
from utils import image_label_2_color


def get_flattened_output(docs):
  flattened_output = []
  annotation_key = 'output'
  for doc in docs:
    flattened_output_item = {annotation_key: []}
    doc_annotation = doc[annotation_key]
    for i, span in enumerate(doc_annotation):
      if len(span['words']) > 1:
        for span_chunk in span['words']:
          flattened_output_item[annotation_key].append(
              {
                  'label': span['label'],
                  'text': span_chunk['text'],
                  'words': [span_chunk]
              }
          )
      else:
        flattened_output_item[annotation_key].append(span)
    flattened_output.append(flattened_output_item)
  return flattened_output


def annotate_image(image_path, annotation_object):
  img = None
  # print("here is the annotation object : ", annotation_object)
  image = Image.open(image_path).convert('RGBA')
  tmp = image.copy()
  label2color = image_label_2_color(annotation_object)
  overlay = Image.new('RGBA', tmp.size, (0, 0, 0)+(0,))
  draw = ImageDraw.Draw(overlay)
  font = ImageFont.load_default()
  # print("***********************", annotation_object['output'])
  predictions = [span['label'] for span in annotation_object['output']]
  try:
    confidences = [span['conf'] for span in annotation_object['output']]
  except Exception as e:
    print("error occured : ",e)
    confidences = [span['words'][0]['conf'] for span in annotation_object['output']]
    
  boxes = [span['words'][0]['box'] for span in annotation_object['output']]
  for prediction, box, confidence in zip(predictions, boxes,confidences):
      draw.rectangle(box, outline=label2color[prediction],
                     width=3, fill=label2color[prediction]+(int(255*0.33),))
      draw.text((box[0] + 10, box[1] - 10), text=prediction+"_"+str(confidence),
                fill=label2color[prediction], font=font)
      # draw.text((box[0] , box[1] ), text=confidence,
      #     fill=label2color[prediction], font=font)

  img = Image.alpha_composite(tmp, overlay)
  img = img.convert("RGB")
  image_name = os.path.basename(image_path)
  image_name = image_name[:image_name.find('.')]
  img.save("/home/sainithish/Downloads/layoutlm/layout_test_harness/new_results1/" + str(image_name) + "_inference.jpg")
